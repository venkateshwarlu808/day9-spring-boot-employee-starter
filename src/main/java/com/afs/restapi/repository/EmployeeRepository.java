package com.afs.restapi.repository;

import com.afs.restapi.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    public List<Employee> findAllByGender(String gender);

    public List<Employee> findAllByAgeLessThan(Integer upperAge);

    public List<Employee> findAllByAgeBetween(Integer minAge, Integer maxAge);

    public List<Employee> findAllByNameContaining(String nameSubstring);
}
